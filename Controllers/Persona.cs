﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSite.Controllers
{
    public class Personas
    {
        public string Nombre { get; set; }
        public int Edad { get; set; }
    }
    public class Persona : Controller
    {
        public ViewResult Calcular(string nombre, int edad)
        {
            var p = new Personas();
            p.Nombre = nombre;
            p.Edad = edad;
            return View(p);
        }
    }
}
